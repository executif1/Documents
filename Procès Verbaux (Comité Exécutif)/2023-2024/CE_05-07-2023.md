# CE DU 5 JUILLET 2023

## 1 - Lecture et adoption odj

### 1.1 - Comment ça va

### 1.2 - Ouverture

Proposition 1 :
- Ouverture du CE par Alex à 19:09
- Dûment appuyée par Kim et acceptée à l’unanimité.
- CE ouvert.

Proposition 2 :
- Adoption de l'ordre du jour par Alex.
- Dûment appuyée par Guillaume.
- Ordre du jour adoptée à l'unanimité.

## 2 - Compétition JDIS (Jeux et Défis Informatiques de Sherbrooke)

### 2.1 - Budget

Budget nécessaire pour le transport et le diner (l'entrée à la compétition est gratuite)

Proposition 3 :
- Armand propose de prendre 50$ dans Pratique format Montréhack et 250$ dans Pratiques compétitions et faire une nouvelle case budgétaire pour financer la compétition organisée par JDIS.
- Dûment appuyée.
- Adoptée à l'unanimité.

## 3 - Initiations

### 3.1 - Planification

Initiations sur une semaine (du mardi au vendredi) :
- Mardi : Tournée des classes, inscriptions et formation des équipes
- Mercredi : Cérémonie d'ouverture avec distributions de marchandises AGEEI
- Jeudi : Journée de défis et option d'aller au Benelux (dépenses non payées)
- Vendredi : Cérémonie des gagnants avec remise de prix et soirée au bar

### 3.2 - Budget

Budget : 10000$

Marchandises et dépenses diverses : 4000$

## 4 - Plans pour l'automne

### 4.1 - Midis-conférences
    
Des professeurs veulent présenter leur recherche.  

Atelier d'initiation à Git I et II par Corinne.
    
### 4.2 - Accessibilité

L'AGEEI appuie l'AESS dans leurs démarches pour rectifier l'accessibilité aux toilettes.
    
### 4.3 - Marchandise AGEEI

Concours ouvert à tous les membres pour proposer un logo pour la marchandise de l'AGEEI.

Pour la fin de la session d'automne idéalement.

## 5 - Varia

### 5.1 - Local

Guillaume a bientôt terminé le ménage du local de l'AGEEI.
     
### 5.1 - BBQ

BBQ le 13 juillet (un petit 5 à 7), dans la cours de l'UQAM.

Tirage de prix, jeux de poche et don de chandails.

## 6 - Fermeture

Proposition 4 :
- Fermeture du CE à 20:14 par Guillaume.
- Dûment appuyée et acceptée à l’unanimité.
- CE fermé.
