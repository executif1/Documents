# Procès verbal de l'assemblée générale de l'AGEEI du 14 avril 2021

## Ordre du Jour

1. Ouverture de l’assemblée
2. Adoption de l’ordre du jour
3. Présentation des états financiers et des activités de la dernière année
4. Nominations
   1. Nomination du/de la président.e
   2. Nomination du/de la secrétaire
   3. Nomination du/de la trésorier/ère
   4. Nomination du/de la VP aux technologies
   5. Nomination du/de la VP aux affaires internes
   6. Nomination du/de la VP aux affaires externes
   7. Nomination du/de la VP aux loisirs
   8. Nomination du/de la VP aux compétitions
5. Varia
6. Fermeture de l’assemblée

## 1. Ouverture de l'assemblée

Ouverture à de l'assemblée 12h40

## 2. Adoption de l'ordre du jour

L'ordre du jour est adopté à l'unanimité.

## 3. Présentation des états financiers et des activités de la dernière année

Armand présente les états financier des activités de la dernière année.
Le budget totale etait de 62235$ on a depenser 40 000$.
Simon propose l'adoption des états financiers.
William appuie la proposition.
Faute de demande les états sont appuyer à l'unanimité.

## 4. Nominations

### 4.1. Nomination du/de la président.e

Lancelot Normand présente son mandat.
Zachary Péloquin présente son mandat.
Lancelot est élu à majorité.

### 4.2. Nomination du/de la secrétaire

Alexandre Lachance se présente.
Simon propose le vote
Alexandre est élu à majorité.

### 4.3. Nomination du/de la trésorier/ère

Kim Joziaque se présente.
Kim est élu à majorité.

### 4.4. Nomination du/de la VP aux technologies

William se présente.
William est élu à majorité.

### 4.5. Nomination du/de la VP aux affaires internes

Nicolas Goulet se présente.
Nicolas est élu à majorité.

### 4.6. Nomination du/de la VP aux affaires externes

Yonis se présente.
Yonis est élu à majorité.

### 4.7. Nomination du/de la VP aux loisirs

Zachary se présente.
Zachary est élu.

### 4.8. Nomination du/de la VP aux compétitions

Alexandre se présente.
Zachary propose la canditature de Simon.
Alexandre est élu.

### 4.9 Officier Première année

Carl Eliot se présente.
Carl Eliot est élue a majorité.

## 5. Varia

Ramener le programme des ambassadeurs.

## 6. Fermeture de l'assemblée

Armand propose la fermeture Simon appuie.
