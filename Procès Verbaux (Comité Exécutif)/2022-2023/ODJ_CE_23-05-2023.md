# Ordre du jour du CE le 23 mai 2023

## Présentation de l’ordre du jour

1. Lecture et adoption de l'ordre du jour
2. Retour rapide sur le northsec
3. Tour à vélo
4. Compte AWS CTF
5. Accès azure pour utiliser les crédits - CTF
6. Calendrier évènements été

## Ouverture

### Proposition 1 : Ouverture du CE

Proposition d'ouverture du CE.
Dûment proposée;
Dûment appuyée.
Ouverture du CE à 17h38.

## Lecture et adoption de l'ordre du jour

- L'ordre du jour est adoptée à l'unanimité.

## Retour rapide sur le northsec

- Résultats excellents de la part de l'équipe de l'UQAM (présence, compétition, participation).
- 550$ en dessous du budget.
- Note pour l'an prochain : Acheter les billets dès le début et préparer les qualifications plus tôt.

## Tour à vélo

- Informations de sécurité relevées par Kim :
  - Une personne avec une formation de premier soin sera présente. Elle doit s'assurer :
    - D'apporter une trousse de premier soin lors de l'évènement.
    - Que tout le nécessaire est dans la trousse, sinon l'acheter et l'ajouter aux dépenses de l'évènement.

### Proposition 2 : Émise par Carl-William

Faire le tour à vélo le 10 juin.
Prix pour les membres :

- 0$ sans location de vélo
- 10$ avec location de vélo

Prix pour les non-membres :

- 10$ sans location de vélo
- 30$ avec location de vélo.

Dûment appuyée; Adoptée à l'unanimité.

## Compte AWS CTF

### Proposition 3 : Émise par Armand

Propose que l'association ait un compte AWS pour les CTF et que le VP compétition ait un accès pour gérer ceux-ci.
Dûment appuyée;
Adoptée à l'unanimité.

## Accès azure pour utiliser les crédits - CTF

### Proposition 4 : Émise par Armand

- Propose d'utiliser les crédits Azure et que tous les membres de l'exécutif y aient accès.

Dûment appuyée. Adoptée à majorité.

## Calendrier évènements été

- Lancement prochain du calendrier sur le site de l'AGEEI.

## Fermeture

### Proposition 5 : Fermeture du CE

Proposition de fermeture du CE.
Dûment proposée;
Dûment appuyée.
Fermeture du CE à 18:18.
